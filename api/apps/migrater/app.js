const appName = 'migrate';
const Archiver = require('archiver'),
    path = require('path'),
    async = require('async'),
    Configs = require('../../../config.js'),
    mongodb = require('mongodb'),
    url = require('url'),
    child_process = require('child_process'),
    crypto = require('crypto'),
    fs = require('fs'),
    multiparty = require('multiparty'),
    AdmZip = require('adm-zip'),
    axios = require('axios'),
    GitHubStrategy = require('passport-github').Strategy,
    passport = require('passport'),
    { Octokit } = require('@octokit/rest'),
    { createPullRequest } = require('octokit-plugin-create-pull-request'),
    { exec } = require('child_process'),
    schemas = require('./schemas.js');
const configs = Configs();
const MongoClient = mongodb.MongoClient;
const dbUri = schemas.dbUri;

let helper, rackspace, saver, emailer, io, restricter, renderer;

function initialize() {
    helper = require('../helper/app');
    saver = require('../saver/app');
    rackspace = require('../rackspacer/app');
    emailer = require('../emailer/app');
    restricter = require('../restricter/app.js');
    renderer = require('../renderer/app.js');
    restarter = require('../restarter/app.js');

    supportedFileTypes = renderer.getSupportedFileTypes();
}

let mongodbDataBase = null;

const getDB = async () => {
    const uri = process.env.MONGODB_URI;
    if (mongodbDataBase) {
        return mongodbDataBase;
    }
    const client = await MongoClient.connect(uri, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });
    const dbName = url.parse(uri).pathname.match(/\/([0-9A-Za-z-_]*)$/)[1];
    mongodbDataBase = client.db(dbName);
    return mongodbDataBase;
};

const getArticle = async (saverOptions) => {
    try {
        const db = await getDB();

        const collection = db.collection('files');
        const documents = await collection.find({
            domain: saverOptions.domain,
            name: saverOptions.file,
        });
        await documents.sort({ dateUpdated: -1 });
        await documents.limit(1);
        const documentArr = await documents.toArray();
        return documentArr[0];
    } catch (e) {
        return null;
    }
};

const checkFileHash = async (saverOptions) => {
    try {
        const article = await getArticle(saverOptions);
        const hash = crypto.createHash('sha256');
        hash.setEncoding('hex');
        hash.write(saverOptions.data);
        hash.end();
        const mongoHash = article.hash;
        return hash.read() !== mongoHash;
    } catch (e) {
        // 		console.log(e);
        return true;
    }
};

const saveFile = async (file, backend = false) => {
    const startPath = backend ? '/app/api/apps/' : '/app/api/libs/';
    if (!file.startsWith(startPath)) return;

    let parsedFileName = file.substring(startPath.length);
    const ext = parsedFileName.split('.').reverse()[0].toLowerCase();
    let renderFileDefaultText = '';

    let fileContents = fs.readFileSync(file, 'utf8');

    if (!fileContents && ext && supportedFileTypes[ext]) {
        renderFileDefaultText = supportedFileTypes[ext].defaultText || '';
        fileContents = renderFileDefaultText;
    }

    if (!parsedFileName) return;
    if (parsedFileName.startsWith('/'))
        parsedFileName = parsedFileName.slice(1);
    if (parsedFileName.endsWith('/')) {
        parsedFileName = parsedFileName + '__hidden';
        fileContents = 'This is hidden file.';
    }

    if (backend) {
        try {
            const splitted = file.split('/');
            parsedFileName =
                splitted[splitted.length - 2] +
                '.' +
                splitted[splitted.length - 1].slice(0, -3);
        } catch (e) {
            return;
        }
    }

    const saverOptions = {
        file: parsedFileName,
        domain: configs.appDomain,
        allowBlank: true,
        data: fileContents,
        title: file.split('\\').pop().split('/').pop(),
        updateFile: false,
        backup: true,
    };

    const restrictions = restricter.getRestrictedFiles();
    if (restrictions.includes(parsedFileName)) return;
    let toUpdate = await checkFileHash(saverOptions);
    backend &&
        (toUpdate =
            /\.api$|\.schemas$|\.app$/.test(parsedFileName) && toUpdate);
    if (toUpdate) {
        console.log(saverOptions.file);
        try {
            await saverUpdateAsync(saverOptions);
            // 		const article = await getArticle(saverOptions);
            // 		console.log(article);
            // 		const db = await getDB();
            // 		await db.collection('files').updateOne(article, {$set:{isBackup: false}});
        } catch (e) {
            console.log(e);
        }
    }
};

const saverUpdateAsync = (saverOptions) => {
    return new Promise((resolve, reject) => {
        saver.update(saverOptions, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
};

const getFiles = async (dirPath, doNotIncludeTopLevelFiles = false) => {
    let files = [];
    const dirFiles = fs.readdirSync(dirPath);
    for (const file of dirFiles) {
        if (file == 'node_modules') continue;
        if (file == '.bash_history') continue;
        if (file == '.heroku') continue;
        if (file == '.git') continue;

        if (fs.statSync(dirPath + '/' + file).isDirectory()) {
            files = [...files, ...(await getFiles(dirPath + '/' + file))];
        } else {
            if (!doNotIncludeTopLevelFiles) {
                files.push(path.join(dirPath, '/', file));
            }
        }
    }
    return files;
};

const getTokensFromSchema = async (shipName) => {
	console.log(shipName);
    const db = await getDB();
    const collection = db.collection('gittokens');
    const documents = await collection
        .find({
            shipName,
        })
        .toArray();
    return documents;
};

const saveGitTokenToSchema = async (url, token, username, shipName) => {
    const db = await getDB();
    const collection = db.collection('gittokens');
    const documents = await collection
        .find({
            url,
            token,
            username,
            shipName,
        })
        .toArray();
    // console.log(documents);
    if (documents.length > 0) return;
    saver.schemaSave({
        schemaName: 'gittoken',
        collectionName: 'GitToken',
        schema: schemas.gitToken,
        dbUri: dbUri,
        modelData: {
            url,
            token,
            username,
            shipName,
        },
    });
};

const checkFilesAndSave = async () => {
    const backendFiles = await getFiles('/app/api/apps', true);
    const otherFiles = await getFiles('/app/api/libs', true);
    // console.log(backendFiles);
    console.log(otherFiles);
    try {
        for (const file of otherFiles) {
            await saveFile(file, false);
        }
        for (const file of backendFiles) {
            await saveFile(file, true);
        }
        // 		restarter.restart();
    } catch (e) {
        console.log(e);
    }
};

module.exports = {
    appName,
    initialize,
    checkFilesAndSave,
    getDB,
    saveGitTokenToSchema,
    getTokensFromSchema,
};